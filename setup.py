################################################################################
#
#  Copyright (c) 2015 christianmeier.information systems and management
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################

import os
import sys
from setuptools import setup
from setuptools import find_packages
from distutils.sysconfig import get_python_lib

# @see https://docs.python.org/2/library/site.html
# site_packages = site.getusersitepackages()
# does not work in virtual environments
# due to site.py of virtualenv's installation is not compatible
# @see https://github.com/pypa/virtualenv/issues/355
for path in sys.path:
  if path.startswith(sys.prefix) and path.endswith('site-packages'):
    site_packages = path
  else:
    site_packages = get_python_lib()

VERSION = '0.3.0dev'

zmspkg_name = 'photodb'
branch_name = 'master'
downld_file = 'https://bitbucket.org/zms3/%s/get/%s.zip'%(zmspkg_name, branch_name)

if 'dev' in VERSION:
  import subprocess
  # retrieve commit hash from local Git repository if available
  rtn = subprocess.Popen("git -C %s describe"%(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))),
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         shell=True, universal_newlines=True)
  rtn = rtn.communicate()[0].strip()
  rtn = rtn.split('-g')
  rtn.reverse()
  
  # otherwise retrieve commit hash from remote Git repository at Bitbucket
  if rtn[0].strip() == '':
    rtn = subprocess.Popen("curl --head " + downld_file,
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                           shell=True, universal_newlines=True)
    rtn = rtn.communicate()[0].strip()
    rtn = rtn.split('.zip')[0].split('filename=zms3-%s-'%zmspkg_name)
    rtn.reverse()
  
  commit_hash = rtn[0].strip()[:7]
  
  VERSION = '%s-%s%s'%(VERSION, branch_name, len(commit_hash)==7 and '-'+commit_hash or '')

INSTALL_REQUIRES = [
# 'ZMS3>=3.1.2',
  'Pillow',
  'python-xmp-toolkit',
  'IPTCInfo',
  'BeautifulSoup4',
  'ftfy',
]

DATA_FILES = [
  (os.path.join(site_packages, 'zms3/photodb/conf'), ['conf/zms3.photodb.metaobj.xml']),
  (os.path.join(site_packages, 'zms3/photodb/conf'), ['conf/zms3.photodb.metacmd.xml']),
  (os.path.join(site_packages, 'zms3/photodb/conf'), ['conf/zms3.photodb.metadict.xml']),
  (os.path.join(site_packages, 'zms3/photodb/conf'), ['conf/zms3.photodb.langdict.xml']),
  (os.path.join(site_packages, 'zms3/photodb/conf'), ['conf/zms3.photodb.example.zip']),
]

PACKAGE_DATA = find_packages()

CLASSIFIERS = [
  'Development Status :: 3 - Alpha',
  'Framework :: Zope2',
  'Programming Language :: Python :: 2.7',
  'Operating System :: OS Independent',
  'Environment :: Web Environment',
  'Topic :: Internet :: WWW/HTTP :: Site Management',
  'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
  'Intended Audience :: Education',
  'Intended Audience :: Science/Research',
  'Intended Audience :: Customer Service',
  'Intended Audience :: End Users/Desktop',
  'Intended Audience :: Healthcare Industry',
  'Intended Audience :: Information Technology',
  'Intended Audience :: Telecommunications Industry',
  'Intended Audience :: Financial and Insurance Industry',
  'License :: OSI Approved :: GNU Affero General Public License v3',
]

setup(
  name                  = 'zms3.photodb',
  description           = 'Read EXIF/XMP/IPTC metadata of Photos and manage Image Galleries',
# long_description      = README,
  version               = VERSION,
  author                = 'Christian Meier',
  author_email          = 'post@christianmeier.info',
  url                   = 'https://bitbucket.org/zms3/photodb',
  download_url          = 'https://bitbucket.org/zms3/photodb/downloads',
  namespace_packages    = ['zms3'],
  packages              = ['zms3.photodb'],
  package_dir           = {'zms3.photodb': 'zms3/photodb'},
  package_data          = {'zms3.photodb': PACKAGE_DATA},
  install_requires      = INSTALL_REQUIRES,
  data_files            = DATA_FILES,
  classifiers           = CLASSIFIERS,
  include_package_data  = True,
  zip_safe              = False,
)
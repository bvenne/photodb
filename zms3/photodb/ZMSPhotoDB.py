################################################################################
#
#  Copyright (c) 2015 christianmeier.information systems and management
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################

from PIL import Image
from PIL.ExifTags import TAGS
from Products.zms import _fileutil
from Products.zms import _globals
from Products.zms import _blobfields
from operator import itemgetter
from bs4 import BeautifulSoup
import watermarker

class ZMSPhotoDB:

  def __init__(self, this):
    
    self.request          = this.REQUEST    
    self.this             = this
    self.thisURLPath      = this.absolute_url()
    self.baseURLPath      = this.getDocumentElement().absolute_url()
    self.thisMaster       = this.breadcrumbs_obj_path(True)[0]
    self.titlealt         = this.attr('titlealt')
    self.title            = this.attr('title')
    self.description      = this.attr('attr_dc_description')
    self.attr_source      = this.attr('attr_source')
    self.attr_path_name   = this.attr('attr_path_name')
    self.attr_exif_data   = this.attr_exif_data
    self.files            = None
    self.items            = []
    self._data            = []
    sizes_sm              = [int(self.this.attr('attr_width_sm')), int(self.this.attr('attr_height_sm'))]
    sizes_md              = [int(self.this.attr('attr_width_md')), int(self.this.attr('attr_height_md'))]
    sizes_lg              = [int(self.this.attr('attr_width_lg')), int(self.this.attr('attr_height_lg'))]
    self.sizes            = [sizes_sm,sizes_md,sizes_lg]

  def getPhotoData(self):
    """
      Return saved photo data from ZMSExifData-recordset
    """
    del self._data[:]
    self._data = self.attr_exif_data.attr('records')
    return self._data    
  
  def setPhotoData(self, reset=False):
    """
      Store photo data to ZMSExifData-recordset
      Empty ZMSExifData-recordset if parameter 'reset' is True
      Return count of affected items
    """
    if not reset:
      val = self._data + self.items
      rtn = len(self.items)
    else:
      val = []
      rtn = len(self.getPhotoData())

    self.this.REQUEST.set('lang', self.this.getPrimaryLanguage())
    self.this.setObjStateModified(self.this.REQUEST)
    self.attr_exif_data.attr('records', val)
    self.this.onChangeObj(self.this.REQUEST)
    self.this.commitObj(self.this.REQUEST,forced=True)
    
    return rtn

  def importPhotoData(self):
    """
      Import all PhotoDB-Items as Image-Objects
    """
    items = self.getPhotoData()

    for item in sorted(items, key=itemgetter('photo_FileName')):
      img_sizes     = self.sizes
      img_format    = 'PNG'
      img_idprefix  = 'attr_images'
      img_filepath  = None
      img_filename  = None
      img_importLg  = False
      cropToFit_sm  = self.this.attr('attr_crop_sm')
      cropToFit_md  = self.this.attr('attr_crop_md')
      cropToFit_lg  = self.this.attr('attr_crop_lg')
      img_existing  = {}
      img_values    = {}
      img_orient    = 1
      img_copyright = ''
      img_artist    = ''
      img_descr     = ''
      img_keywords  = ''
      img_year      = ''
      
      if 'photo_FilePath' in item and 'photo_FileName' in item:
        filepath = item['photo_FilePath'].strip()
        filename = item['photo_FileName'].strip()
        if (filepath!='' and filename!=''):
          img_filepath = str(filepath)
          img_filename = str(filename)
      
      if 'exif_Copyright' in item:
        img_copyright = str(item['exif_Copyright'])
      
      if 'exif_Orientation' in item:
        try:
          img_orient = int(item['exif_Orientation'])
        except:
          pass
 
      if 'exif_DateTimeOriginal' in item:
        year = str(item['exif_DateTimeOriginal']).split(':',1)
        if len(year)>0:
          img_year = year[0]
        
      if 'exif_Artist' in item:
        img_artist  = str(item['exif_Artist']).strip()
        
      if 'exif_ImageDescription' in item:
        img_descr = str(item['exif_ImageDescription']).strip()

      if 'xmp_Subject' in item:
        img_keywords = str(item['xmp_Subject']).strip()
       
      if (int(self.this.attr('attr_import_large'))==1) and (self.this.getConfProperty('ZMSGraphic.superres',0)==1):
        img_importLg  = True
      
      img_specials  = 'alt=\042%s\042 title=\042%s\042'%(img_filename,img_copyright and '&copy '+img_copyright or img_descr)
      img_text      = '%s\n\nDateiname: %s\n%s\n%s'%(img_descr, \
                                                     img_filename, \
                                                     img_keywords and '\nStichworte:\n%s'%img_keywords or '', \
                                                     img_copyright and '\n&copy %s'%img_copyright or '')

      # save data of PhotoDB-Items to primary language of Image-Objects
      lang = self.this.getPrimaryLanguage()
      self.request.set('lang',lang)
      
      # prepare check if Image-Object with current filename already exists
      for img in self.this.getChildNodes(self.request,['ZMSGraphic']):
        key = img.attr('img').getFilename().rsplit('.',1)[0].replace('_sm','')
        val = img
        img_existing[key] = val
      filename = img_filename.rsplit('.',1)[0]
      for ch in [ '+', '%', ' ', '!', '?', '#', '"', '(', ')', '&' ]:
        filename.replace(ch,'') 
      filename = filename.replace(" ","")
      
      # modify image descriptions only
      if filename in img_existing:
        self.request.set('img_attrs_spec',img_specials)
        self.request.set('text_'+lang,img_text)
        self.request.set('align','Left')
        img_existing[filename].manage_changeProperties(lang, self.request)
      
      # create a new Image-Object
      else:
        img_sm, img_md, img_lg = self._createImageVariants(img_filepath, img_filename, \
                                                           img_sizes, img_orient, \
                                                           img_format, img_importLg, \
                                                           cropToFit_sm, cropToFit_md, cropToFit_lg)
      
        img_values = {'id_prefix':img_idprefix, 'img':img_sm, 'imghires':img_md, \
                      'img_attrs_spec':img_specials, 'text':img_text, 'align':'Left'}
        
        if img_lg is not None:
          img_values['imgsuperres'] = img_lg
        
        self.this.manage_addZMSCustom('ZMSGraphic', img_values, self.request)
      
    return len(items)

  def readMetaData(self, EXIF=True, XMP=True, IPTC=False):
    """
      Read metadata from image files
      EXIF: http://en.wikipedia.org/wiki/Exchangeable_image_file_format
      XMP: http://en.wikipedia.org/wiki/Extensible_Metadata_Platform
      IPTC: http://en.wikipedia.org/wiki/International_Press_Telecommunications_Council
    """
    if self.files is None:
      self._readFilesFromDefinedSource()

    for f in self.files:
      filename      = f['filename']
      filepath      = f['local_filename']
      filesize      = f['size']
      filedatetime  = f['mtime']
      imagemetadata = {}
      orientation   = 1

      # Extract EXIF metadata if available
      if EXIF:
        try:
          img_file = Image.open(filepath)
          img_exif = img_file._getexif()
          if img_exif is not None:
            for tag, value in img_exif.items():
              decoded = TAGS.get(tag, tag)
              if type(value) is unicode:
                value = value.encode('latin-1','xmlcharrefreplace')
              imagemetadata['exif_'+decoded] = str(value)
              if decoded == 'Orientation': orientation = int(value)        
        except:
          _globals.writeError(self, "[readMetaData] %s: Failure on processing EXIF metadata!"%filename)

      # Extract XMP metadata if available
      if XMP:
        try:
          img_file  = open(filepath)
          img_xmp   = img_file.read()
          xmp_start = img_xmp.find('<x:xmpmeta')
          xmp_end   = img_xmp.find('</x:xmpmeta')
          xmp_str   = img_xmp[xmp_start:xmp_end+12]
          xmp_xml   = BeautifulSoup(xmp_str)
          keywords  = []
          for subject in xmp_xml('dc:subject'):
            for keyword in subject.find('rdf:bag').find_all('rdf:li'):
              keywords.append(str(keyword.get_text().strip().encode('utf-8','xmlcharrefreplace')))
          imagemetadata['xmp_Subject'] = "; ".join(sorted(keywords))
          img_file.close()
        except:
          _globals.writeError(self, "[readMetaData] %s: Failure on processing XMP metadata!"%filename)
      
      # Extract IPTC metadata if available
      if IPTC:
        try:
          from iptcinfo import IPTCInfo
          img_iptc = IPTCInfo(filepath)
          if len(img_iptc.data) > 3:
            # TODO: read and store IPTC metadata
            print img_iptc.keywords
        except:
          _globals.writeError(self, "[readMetaData] %s: Failure on processing IPTC metadata!"%filename)  
      
      # Append additional metadata
      imagemetadata['photo_FileName'] = filename
      imagemetadata['photo_FilePath'] = filepath
      imagemetadata['photo_FileSize'] = filesize
      imagemetadata['photo_FileDateTime'] = filedatetime
      imagemetadata['photo_Preview'] = self._createImagePreview(filepath, orientation)

      self.items.append(imagemetadata)

     
  def _readFilesFromDefinedSource(self, recursive=False):
    """
      Return JPEG/TIFF image files
      from defined source at ZMSPhotoDB-metaobj
      if filename is not available yet in ZMSExifData-recordset
    """
    if self.files is not None: 
      del self.files[:]
 
    photo_data = self.getPhotoData()
    existing = []
    for data in photo_data:
      existing.append(data['photo_FileName'])
 
    if self.attr_source in ['Upload', 'Dropbox']:
      raise NotImplementedError('Not implemented.')
    
    elif self.attr_source in ['Filesystem']:
      if self.attr_path_name is None or self.attr_path_name.strip()=='':
        raise OSError('No filesystem path specified.')
      
      files = _fileutil.readPath(self.attr_path_name, data=False, recursive=recursive)
      files =  filter(lambda x: x['filename'] not in existing, files)
      files =  filter(lambda x: x['filename'].lower().endswith('.jpg') or \
                                x['filename'].lower().endswith('.jpeg') or \
                                x['filename'].lower().endswith('.tif') or \
                                x['filename'].lower().endswith('.tiff'), \
                                files)
    else:
      files = []
    
    self.files = files
    return self.files
  
  def _createImagePreview(self, filePathOrData=None, orientation=1, encode='base64'):
    """
      Create a preview 32x32 base64-encoded by default
      for storing at ZMSExifData-recordset
      and rendering as inline image/png
    """    
    if isinstance(filePathOrData, _blobfields.MyImage):
      from io import BytesIO
      img = Image.open(BytesIO(filePathOrData.getData()))
    elif isinstance(filePathOrData, basestring):
      img = Image.open(filePathOrData)
    else:
      raise IOError('No Image found.')
     
    img = img.convert("RGB")
    img.thumbnail((32,32))
    
    # flip horizontal if needed
    if int(orientation)==3: 
      img = img.rotate(180)
    if int(orientation)==6: 
      img = img.rotate(270)
      
    img.save('/tmp/img_pv.png')
    f = open('/tmp/img_pv.png', 'r')
    preview = f.read()
    f.close()
    
    if encode == 'base64':
      import base64
      return base64.b64encode(preview)
    else:
      return preview

  def _createImageVariants(self, filepath=None, filename='image.jpg', \
                           sizes=[[125,125],[600,600],[1024,768]], orientation=1, \
                           fmt='JPG', importLarge=False, \
                           cropToFit_sm=True, cropToFit_md=True, cropToFit_lg=False):
    """
      Create small, medium and large variants
      of the given image file and parameters
    """
    if filepath is None:
      raise IOError('No image found.')
    
    file_name = filename.rsplit('.',1)[0]
    file_ext  = fmt.lower()
    
    img = Image.open(filepath)
    img = img.convert("RGB")
     
    # flip horizontal if needed
    if int(orientation)==3: 
      img = img.rotate(180)
    if int(orientation)==6: 
      img = img.rotate(270)

    # calculate sizes to keep aspect ratio
    src_width, src_height       = img.size
    dst_width_sm, dst_height_sm = sizes[0]
    dst_width_md, dst_height_md = sizes[1]
    dst_width_lg, dst_height_lg = sizes[2]
        
    # for landscape source
    if src_width > src_height:
      aspect_ratio = float(src_width)/float(src_height)
      size_sm      = (dst_width_sm,int(dst_width_sm/aspect_ratio))
      size_md      = (dst_width_md,int(dst_width_md/aspect_ratio))
      size_lg      = (dst_width_lg,int(dst_width_lg/aspect_ratio))
    # for portrait source
    else:
      aspect_ratio = float(src_height)/float(src_width)
      size_sm      = (int(dst_height_sm/aspect_ratio),dst_height_sm)
      size_md      = (int(dst_height_md/aspect_ratio),dst_height_md)
      size_lg      = (int(dst_height_lg/aspect_ratio),dst_height_lg)
        
    # SMALL variant
    if cropToFit_sm:
      img_sm = self._cropToFit(img, src_width, src_height, dst_width_sm, dst_height_sm)
    else:
      img_sm = img.resize(size_sm, Image.LANCZOS)

    img_sm.save('/tmp/img_sm.png')
    f = open('/tmp/img_sm.png', 'r')
    small = f.read()
    f.close()
    small = self.this.ImageFromData(small, file_name+'_sm.'+file_ext, mediadbStorable=True)

    # MEDIUM variant
    if cropToFit_md:
      img_md = self._cropToFit(img, src_width, src_height, dst_width_md, dst_height_md)
    else:
      img_md = img.resize(size_md, Image.LANCZOS)

    img_md.save('/tmp/img_md.png')
    f = open('/tmp/img_md.png', 'r')
    medium = f.read()
    f.close()
    medium = self.this.ImageFromData(medium, file_name+'_md.'+file_ext, mediadbStorable=True)
    
    if not importLarge:
      return small, medium, None
    
    # LARGE variant
    if cropToFit_lg:
      img_lg = self._cropToFit(img, src_width, src_height, dst_width_lg, dst_height_lg)
    else:
      img_lg = img.resize(size_lg, Image.LANCZOS)

    # TODO: watermarker.insertWatermark(img_lg, "Test", color=(255,255,255), opacity=0.4, font_path='/Users/cmeier/Desktop/OpenSans-Light.ttf', font_size=100)
    
    img_lg.save('/tmp/img_lg.png')
    f = open('/tmp/img_lg.png', 'r')
    large = f.read()
    f.close()
    large = self.this.ImageFromData(large, file_name+'_lg.'+file_ext, mediadbStorable=True)
    
    return small, medium, large

  def _cropToFit(self, img=None, src_width=0, src_height=0, dst_width=0, dst_height=0):
    """
      Return a cropped and resized version
      of the given image according to the given sizes
    """    
    if (img is None) or (src_width==0) or (src_height==0) or (dst_width==0) or (dst_height==0):
      return Image.new('RGB', (125,125))

    src_width   = max(src_width,dst_width)
    src_height  = max(src_height,dst_height)
    dst_width   = min(src_width,dst_width)
    dst_height  = min(src_height,dst_height)
    
    crop_ratio_w    = float(src_width)/float(dst_width)
    crop_ratio_h    = float(src_height)/float(dst_height)
    
    # for landscape result
    if src_width > src_height:
      size = (int(src_width/crop_ratio_h), dst_height)
    # for portrait result
    else:
      size = (dst_width, int(src_height/crop_ratio_w))
  
    img = img.resize(size, Image.LANCZOS)
    width, height = img.size
    
    if width > dst_width:
      left = (width - dst_width)/2
      upper = 0
      right = (width + dst_width)/2
      bottom = dst_height
      img = img.crop((left, upper, right, bottom))        
      
    if height > dst_height:
      left = 0
      upper = (height - dst_height)/2
      right = dst_width
      bottom = (height + dst_height)/2
      img = img.crop((left, upper, right, bottom))
    
    return img

